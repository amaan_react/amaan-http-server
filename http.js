const http = require('http'); 
const fs = require('fs')
const sc = require('http').STATUS_CODES

const server = http.createServer(function (req, res) { 

    const reqUrl = req.url
    
    switch(reqUrl) {
        
        case '/html':
            fs.readFile('get.html', 'utf-8',(err, data)=>{
                if (err){
                    res.writeHead(400, {'content-type': 'text/html'})
                    res.write('<h1>No HTML File Found !!!</h1>')
                    res.end()
                    return
                } else {
                    res.writeHead(200, {'content-type': 'text/html'})
                    res.write(data)
                    res.end()
                    return
                }

            })
            return


        case '/json':
            fs.readFile('get.json', 'utf-8',(err, data)=>{
                if (err){
                    res.writeHead(400, {'content-type': 'text/html'})
                    res.write('<h1>No json File Found !!!</h1>')
                    res.end()
                    return
                } else {
                    res.writeHead(200, {'content-type': 'text/json'})
                   
                    res.write(data)
                    res.end()
                    return
                }

            })
            return
    }
    if (reqUrl.includes("/status/")) {
        let code = sc[reqUrl.slice(8)]
        res.writeHead(200, { "Content-type": "text/plain" });
        res.write(code)
        res.end();
    }

    else if (reqUrl.includes("/delay/")) {
        let delay = reqUrl.slice(7)
        setTimeout(() => { 
        res.writeHead(200, { "Content-type": "text/plain" });
        res.write("Returning a success response after " + delay)
        res.end();
        }, delay);
    }
    

});

server.listen(4422)

console.log('Node.js web server at port 4422 is running..')


